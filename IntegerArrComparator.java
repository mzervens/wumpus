package pkg2d;

import java.util.Comparator;


public class IntegerArrComparator implements Comparator<Integer []>
{
    @Override
    public int compare(Integer [] x, Integer [] y)
    {

        if (x[0] < y[0])
        {
            return -1;
        }
        if (x[0] > y[0])
        {
            return 1;
        }
        return 0;
    }

}
