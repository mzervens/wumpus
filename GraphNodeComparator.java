package pkg2d;

import java.util.Comparator;


public class GraphNodeComparator implements Comparator<GraphNode>
{
    @Override
    public int compare(GraphNode x, GraphNode y)
    {

        if (x.data[0] < y.data[0])
        {
            return -1;
        }
        if (x.data[0] > y.data[0])
        {
            return 1;
        }
        return 0;
    }

}
