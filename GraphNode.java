package pkg2d;


public class GraphNode 
{
    Integer data [];
    GraphNode link;
    
    public GraphNode()
    {
        this(3);

    }
    public GraphNode(int size)
    {
        data = new Integer[size];
        link = null;
    }
    
    public boolean dataEquals(Object obj [])
    {
        if(obj[0] == data[1] && obj[1] == data[2])
        {
            return true;
        }
        else
        {
            return false;
        }
    
    }
 
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof GraphNode)
        {
          GraphNode c = (GraphNode)o;
          
          if(c.data.length == data.length)
          {
            for(int i = 0; i < data.length; i++)
            {
                if(data[i] != c.data[i])
                {
                    return false;
                }

            }
            return true;
          }
          
        }
        return false;
    }

}