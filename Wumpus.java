
package pkg2d;

import java.util.Random;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Stack;




public class Wumpus extends BasicGame
{
    private static boolean alive = true;
    private static String mapPath = System.getProperty("user.dir") + "/map";
    private static String movementPath = System.getProperty("user.dir") + "/movement";
    private static String trackingPath = System.getProperty("user.dir") + "/paths";
    private static BufferedWriter fWriter;
    private static BufferedWriter pathTracking;
    private static BufferedReader movementReader;
    private static String map [][];
    private static String knownMap [][];
    private static Integer dangerMap [][];
    private static Integer distanceMap [][];
    private static Integer currentPosition [] = new Integer [2];
    private static Integer startCoordinates [] = new Integer [2];
    private static PriorityQueue<Integer []> borderFields = new PriorityQueue<>(10, new IntegerArrComparator());
    private static HashMap <String, String> mapConfiguration = new HashMap<>();
    
    
    private static Integer cellPixels = 64;
    private static long updateTime;
    private static String readMovement;
    private static int updateDelay = 1;
    private static HashSet<String> visitedSquares = new HashSet<>();
    
    public Wumpus(String title)
    {
        super(title);
    }
    
    private static void initializeMaps() throws Exception
    {
        BufferedReader fReader = new BufferedReader(new FileReader(mapPath));
        fWriter = new BufferedWriter(new FileWriter(movementPath));
        pathTracking = new BufferedWriter(new FileWriter(trackingPath));
        String fLine;

        while((fLine = fReader.readLine()) != null)
        {
            if(fLine.contains("#") || !(fLine.contains("=>"))) 
            {
                continue;
            }
            
            String parts [] = fLine.split("=>");
            
            if(parts.length == 2)
            {
                mapConfiguration.put(parts[0].trim(), parts[1].trim());
            }
        }
        
        int mapSize = Integer.parseInt(mapConfiguration.get("size"));
        map = new String [mapSize][];
        knownMap = new String [mapSize][];
        dangerMap = new Integer [mapSize][];
        distanceMap = new Integer[mapSize][];
        
        for(int i = 0; i < mapSize; i++)
        {
            map[i] = new String [mapSize];
            knownMap[i] = new String [mapSize];
            dangerMap[i] = new Integer [mapSize];
            distanceMap[i] = new Integer [mapSize];
        }
        
        for(int i = 0; i < mapSize; i++)
        {
            for(int n = 0; n < mapSize; n++)
            {
                map[i][n] = mapConfiguration.get(Integer.toString(i) + Integer.toString(n));
                knownMap[i][n] = "unknown";
                
                if(map[i][n].contains("start"))
                {
                    currentPosition[0] = i;
                    currentPosition[1] = n;
                    
                    startCoordinates[0] = i;
                    startCoordinates[1] = n;
                    
                    knownMap[i][n] = map[i][n];
                    
                    dangerMap[i][n] = 0;
                } 
                
            }
        }

        

    }
    
    private static void printMap() throws Exception
    {
        printMap(map, false);
    }
    private static void printMap(Object map [][], boolean logPath) throws Exception
    {

        for(int i = map.length; i > 0; i--)
        {
            for(int n = 0; n < map[i - 1].length; n++)
            {
                System.out.print(map[i - 1][n] + "  ");
                if(logPath)
                {
                    pathTracking.write(map[i - 1][n] + "  ");
                }
            }
            System.out.println("");
            if(logPath)
            {
                pathTracking.newLine();
            }
        }

    }
    
    private static int [][] findLegalMoves()
    {
        return findLegalMoves(null);
    }
    private static int [][] findLegalMoves(Integer position [])
    {
        int moves [][];
        int arrSize = 4;
        int movesIndex = 0;
        
        if(position == null)
        {
            position = currentPosition;
        }
        
        if(position[0] == 0 || position[0] == map.length - 1)
        {
            arrSize--;
        }
        if(position[1] == 0 || position[1] == map.length - 1)
        {
            arrSize--;
        }
        
        moves = new int [arrSize][2];

        if(position[0] - 1 >= 0)
        {
            moves[movesIndex][0] = position[0] - 1;
            moves[movesIndex][1] = position[1];
            
            movesIndex++;
        }
        if(position[0] + 1 < map.length)
        {
            moves[movesIndex][0] = position[0] + 1;
            moves[movesIndex][1] = position[1];
            
            movesIndex++;
        }
        if(position[1] - 1 >= 0)
        {
            moves[movesIndex][0] = position[0];
            moves[movesIndex][1] = position[1] - 1;
            
            movesIndex++;
        }
        if(position[1] + 1 < map.length)
        {
            moves[movesIndex][0] = position[0];
            moves[movesIndex][1] = position[1] + 1;

        }
        
        return moves;  
    }
    
    private static void initializeHighValuePositions()
    {
        ArrayList<ArrayList<Integer>> highValuePosition = new ArrayList<>();
        borderFields.clear();

        
        for(int i = 0; i < knownMap.length; i++)
        {
            for(int n = 0; n < knownMap.length; n++)
            {
                if(knownMap[i][n].equals("unknown")) 
                {
                    continue;
                }
                else
                {
                    ArrayList<Integer> coordinates = new ArrayList<>();
                    coordinates.add(0, i);
                    coordinates.add(1, n);
                    
                    highValuePosition.add(coordinates);
                }
            
            }
            
        
        }
        
   /*     if(val == 3)
        {
            for(int i = 0; i < highValuePosition.size(); i++)
            {
                
                System.err.println("explored possition: " + highValuePosition.get(i).get(0)+ " " + highValuePosition.get(i).get(1));
            
            }

        }
     */   
        for(int i = 0; i < highValuePosition.size(); i++)
        {
            int legalMoves [][] = findLegalMoves(new Integer []{highValuePosition.get(i).get(0), highValuePosition.get(i).get(1)});

            for(int n = 0; n < legalMoves.length; n++)
            {
                if(knownMap[legalMoves[n][0]][legalMoves[n][1]].equals("unknown"))
                {
                    int dangers = getPositionDangerCount(legalMoves[n]);
                    dangerMap[legalMoves[n][0]][legalMoves[n][1]] = dangers;
                    Integer element [] = new Integer[3];
                    element[0] = dangers;
                    element[1] = legalMoves[n][0];
                    element[2] = legalMoves[n][1];
                    
                    borderFields.add(element);

                }
            }
        }
        
        Integer nextTarget [] = borderFields.peek();
        if(nextTarget != null && nextTarget[0] > 0)
        {
            PriorityQueue<Integer []> rescanned = new PriorityQueue<>(10, new IntegerArrComparator());
            nextTarget = borderFields.remove();
            int size = borderFields.size();
            for(int i = 0; i < size; i++)
            {
                int legalMoves [][] = findLegalMoves(new Integer [] {nextTarget[1], nextTarget[2]});
                for(int n = 0; n < legalMoves.length; n++)
                {
                    int legalMoveMoves [][] = findLegalMoves(new Integer [] {legalMoves[n][0], legalMoves[n][1]});
                    if(knownMap[legalMoves[n][0]][legalMoves[n][1]].equals("wind") || knownMap[legalMoves[n][0]][legalMoves[n][1]].equals("smell"))
                    {
                        String indicator = "";
                        if(knownMap[legalMoves[n][0]][legalMoves[n][1]].equals("wind"))
                        {
                            indicator = "wind";
                        }
                        else
                        {
                            indicator = "smell";
                        }
                        boolean allKnown = true;
                        for(int s = 0; s < legalMoveMoves.length; s++)
                        {
                            if(legalMoveMoves[s][0] == nextTarget[1] && legalMoveMoves[s][1] == nextTarget[2])
                            {
                                continue;
                            }
                            if(knownMap[legalMoveMoves[s][0]][legalMoveMoves[s][1]].contains("unknown") || knownMap[legalMoveMoves[s][0]][legalMoveMoves[s][1]].contains("hole") || knownMap[legalMoveMoves[s][0]][legalMoveMoves[s][1]].contains("wumpus"))
                            {
                                allKnown = false;
                                break;
                            }
                        
                        }
                        
                        if(allKnown == true)
                        {
                            dangerMap[nextTarget[1]][nextTarget[2]] = 100;
                            if(indicator.equals("wind"))
                            {
                                knownMap[nextTarget[1]][nextTarget[2]] = "hole";
                            }
                            else
                            {
                                knownMap[nextTarget[1]][nextTarget[2]] = "wumpus";
                            }
            /*                for(int m = 0; m < legalMoves.length; m++)
                            {
                                if(knownMap[legalMoves[m][0]][legalMoves[m][1]].equals("unknown"))
                                {
                                        continue;
                                    }
                                    else if(knownMap[legalMoves[m][0]][legalMoves[m][1]].contains(indicator))
                                    {
                                        knownMap[legalMoves[m][0]][legalMoves[m][1]] = knownMap[legalMoves[m][0]][legalMoves[m][1]].replace(indicator, "");
                                    }
                                }
                            }
                            */
                            nextTarget[0] = 100;
                            break;
                        }
                    
                    
                    }
                
                
                }
                rescanned.add(nextTarget);
                nextTarget = borderFields.remove();
            
            }
        
            borderFields = rescanned;
        
        }
        

    }
    
    private static int getPositionDangerCount(int position [])
    {
        int dangers = 0;
        boolean exit = false;
        int legalMoves [][] = findLegalMoves(new Integer[] {position[0], position[1]});
        
        for(int i = 0; i < legalMoves.length; i++)
        {
            String currentlyKnown = knownMap[legalMoves[i][0]][legalMoves[i][1]];
            String currentlyKnownSplit [] = currentlyKnown.split(",");

            for(int n = 0; n < currentlyKnownSplit.length; n++)
            {
                if(currentlyKnownSplit[n].contains("wind"))
                {
                    dangers++;
                }
                else if(currentlyKnownSplit[n].contains("smell"))
                {
                    dangers++;
                }
                else if(currentlyKnownSplit[n].contains("wumpus"))
                {
                    dangers = 100;
                }
                else if(currentlyKnownSplit[n].contains("hole"))
                {
                    dangers = 100;
                }
      /*          else if(!(currentlyKnownSplit[n].contains("unknown")))
                {
                    dangers--;
                
                }
                */
                else if(currentlyKnownSplit[n].contains("null"))
                {
                    dangers = 0;
                    exit = true;
                    break;
                }
                else if(currentlyKnownSplit[n].contains("start"))
                {
                    dangers = 0;
                    exit = true;
                    break;

                }
                
            }
            if(exit)
            {
                break;
            }

        }

        return dangers;
    
    }
    
    private static Integer [] pickNextMove()
    {
        Integer move [] = new Integer [2];
        Integer queueHead [] = borderFields.poll();
        if(queueHead == null)
        {
            return null;
        }
        else
        {
            move[0] = queueHead[1];
            move[1] = queueHead[2];
        }
        return move;
    
    }
    
    private static PriorityQueue<GraphNode> initializeDistanceMap(Integer target [])
    {
        PriorityQueue<GraphNode> priorityQueue = new PriorityQueue<>(10, new GraphNodeComparator());
        for(int i = 0; i < distanceMap.length; i++)
        {
            for(int n = 0; n < distanceMap[i].length; n++)
            {
                if(knownMap[i][n].contains("unknown")) 
                {   
                    if(!(i == target[0] && n == target[1]))
                    {
                        continue;
                    }
                }
                if(currentPosition[0] == i && currentPosition[1] == n)
                {
                    distanceMap[i][n] = 0;
                }
                else
                {
                    distanceMap[i][n] = Integer.MAX_VALUE;
                }

                GraphNode element = new GraphNode();
                element.data[0] = distanceMap[i][n];
                element.data[1] = i;
                element.data[2] = n;
                
                priorityQueue.add(element);
            }
        
        }
        
        return priorityQueue;
    }
    
    private static Stack<GraphNode> findPath(Integer target [])
    {
        PriorityQueue<GraphNode> queue = initializeDistanceMap(target);
        Stack<GraphNode> path = new Stack<>();
        GraphNode tail = null;
        
        while(!(queue.isEmpty()))
        {

            GraphNode currentVertex = queue.poll();
            if(currentVertex.dataEquals(target))
            {
                tail = currentVertex;
                break;
            }
            
            int neighbours [][] = findLegalMoves(new Integer[]{currentVertex.data[1], currentVertex.data[2]});
            for(int i = 0; i < neighbours.length; i++)
            {
                if(knownMap[neighbours[i][0]][neighbours[i][1]].equals("unknown"))
                {
                    if(!(neighbours[i][0] == target[0] && neighbours[i][1] == target[1]))
                    continue;
                }
                if(knownMap[neighbours[i][0]][neighbours[i][1]].equals("wumpus"))
                {
                    continue;
                }
                if(knownMap[neighbours[i][0]][neighbours[i][1]].equals("hole"))
                {
                    continue;
                }
                
                int dist = currentVertex.data[0] + distanceBetweenPoints(new int []{currentVertex.data[1], currentVertex.data[2]}, new int []{neighbours[i][0], neighbours[i][1]});
                
                if(dist < distanceMap[neighbours[i][0]][neighbours[i][1]])
                {
                    GraphNode element = new GraphNode();
                    element.data[0] = distanceMap[neighbours[i][0]][neighbours[i][1]];
                    element.data[1] = neighbours[i][0];
                    element.data[2] = neighbours[i][1];
                    
                    queue.remove(element);
                    
                    element.data[0] = dist;
                    element.link = currentVertex;
                    
                    
                    queue.add(element);
                    
                    
                }
            }
            
            
        }
        
        while(tail.link != null)
        {
            path.push(tail);
            tail = tail.link;
        
        }
        
        return path;
        
    }
    
    private static int distanceBetweenPoints(int point1 [], int point2 [])
    {
        int distance = Math.abs(point1[0] - point2[0]) + Math.abs(point1[1] - point2[1]);
        return distance;
    }
    
    private static void move(Integer position []) throws Exception
    {
        logMovement(currentPosition[0] + " " + currentPosition[1] + " => " + position[0] + " " + position[1]);
        currentPosition[0] = position[0];
        currentPosition[1] = position[1];
        
        if(knownMap[position[0]][position[1]].equals("unknown"))
        {
            knownMap[position[0]][position[1]] = map[position[0]][position[1]];
            if(map[position[0]][position[1]].contains("hole") || map[position[0]][position[1]].contains("wumpus"))
            {
                alive = false;
              //  dangerMap[position[0]][position[1]] = 100;
            }
            else
            {
                dangerMap[position[0]][position[1]] = 0;
            }
        }

    }

    private static void logMovement(String data) throws Exception
    {
        fWriter.write(data);
        fWriter.newLine();

    }
    
    private static void logPath(String data, boolean nL) throws Exception
    {
        pathTracking.write(data);
        if(nL)
        {
            pathTracking.newLine();
        }
    }


    public void init(GameContainer gcontainer) throws SlickException            // Called once during the application initialization process 
    {                                                                           // eg loading screen

        try
        {
            initializeMaps();
        }
        catch(Exception e)
        {
            System.err.println("Could not initialize");
            return;
        }
        
        
        while(alive != false || !(map[currentPosition[0]][currentPosition[1]].contains("gold")))
        {

            initializeHighValuePositions();

            Integer nextTarget [] = pickNextMove();
            if(nextTarget == null)
            {
                break;
            }
        //    System.out.println(nextTarget[0] + " " + nextTarget[1]);

            Stack<GraphNode> path = findPath(nextTarget);
 
            while(!(path.empty()))
            {
                if(alive == false)
                {
                    break;
                }
                GraphNode element = path.pop();

                try
                {
                    logPath(element.data[1] + " " + element.data[2] + "  ", false);
                    move(new Integer [] {element.data[1], element.data[2]});
                }
                catch(Exception e)
                {
                    System.err.println("There were problems moving");
                    return;
                }

            }
            try
            {
                logPath("", true);
            }
            catch(Exception e)
            {
                System.err.println("There were problems logging path");
                return;
            
            }
            
            if(alive == false)
            {
                break;
            }
            else if(map[currentPosition[0]][currentPosition[1]].contains("gold"))
            {
                break;
            
            }

        }
        
        try
        {
            printMap(dangerMap, true);
            pathTracking.close();
            fWriter.close();
            movementReader = new BufferedReader(new FileReader(movementPath));
            String line = movementReader.readLine();
            if(line != null)
            {
                String lineParts [] = line.split(" => ");
                readMovement = lineParts[1];
            
            }
        }
        catch(Exception e)
        {
            System.err.println("There were problems writing to the map file");
            return;
        }
        
        currentPosition[0] = startCoordinates[0];
        currentPosition[1] = startCoordinates[1];
        
        Calendar updateCal = Calendar.getInstance();
        updateCal.add(Calendar.SECOND, updateDelay);
        
        updateTime = updateCal.getTimeInMillis();
        visitedSquares.add(String.valueOf(currentPosition[0]) + String.valueOf(currentPosition[1]));
        
 
    }
    

    public void update(GameContainer gcontainer, int delta) throws SlickException // Updates games logic eg move character across the screen by 100px
    {
        Calendar now = Calendar.getInstance();
        if(now.getTimeInMillis() < updateTime)
        {
            return;
        }
        
        String movementCoords [] = readMovement.split(" ");
        currentPosition[0] = Integer.parseInt(movementCoords[0]);
        currentPosition[1] = Integer.parseInt(movementCoords[1]);
        
        visitedSquares.add(movementCoords[0] + movementCoords[1]);
        
        now.add(Calendar.SECOND, updateDelay);
        updateTime = now.getTimeInMillis();

        try
        {
            String line = movementReader.readLine();
            if(line != null)
            {
                String lineParts [] = line.split(" => ");
                readMovement = lineParts[1];
            }
        }
        catch(Exception e)
        {
            System.err.println("Problems reading movement file");
            return;
        }

    }


    public void render(GameContainer gcontainer, Graphics graphics) throws SlickException // Actually render changes to the screen
    {
        Integer startX = 0 + 150;
        Integer startY = gcontainer.getHeight() - 150;
        Color current = new Color(32, 178, 170);
        Color white = new Color(255, 255, 255);
        Color grey = new Color(190, 190, 190);
        
        for(int i = 0; i < map.length; i++)
        {
            for(int n = 0; n < map[i].length; n++)
            {
                if(currentPosition[0] == i &&  currentPosition[1] == n)
                {
                    graphics.setColor(current);
                    graphics.fillRect(startX, startY, cellPixels, cellPixels);
                    graphics.setColor(white);

                }
                else if(visitedSquares.contains(String.valueOf(i) + String.valueOf(n)))
                {
                    graphics.setColor(grey);
                    graphics.fillRect(startX, startY, cellPixels, cellPixels);
                    graphics.setColor(white);

                }
                
                
                graphics.setColor(white);
                graphics.drawRect(startX, startY, cellPixels, cellPixels);

                
                if(map[i][n].contains("start"))
                {
                    graphics.drawString("Start", startX + 10, startY + cellPixels / (3));
                }
                else if(map[i][n].contains("gold"))
                {
                    graphics.drawString("Gold", startX + 15, startY + cellPixels / (3));
                
                }
                else if(map[i][n].contains("wumpus"))
                {
                    graphics.drawString("Wumpus", startX + 5, startY + cellPixels / (3));
                
                }
                else if(map[i][n].contains("hole"))
                {
                    graphics.drawString("Hole", startX + 15, startY + cellPixels / (3));
                
                }
                else if(map[i][n].contains("wind") && map[i][n].contains("smell"))
                {
                    graphics.drawString("Wind,", startX + 15, startY + cellPixels / (6));
                    graphics.drawString("Smell", startX + 15, startY + cellPixels / (2));
                }
                else if(map[i][n].contains("wind"))
                {
                    graphics.drawString("Wind", startX + 15, startY + cellPixels / (3));
                }
                else if(map[i][n].contains("smell"))
                {
                    graphics.drawString("Smell", startX + 12, startY + cellPixels / (3));
                }
                

                
                

                startX += cellPixels;
            }
            startX = 0 + 150;
            startY -= cellPixels;
        }



    }

    public static void main(String args []) 
    {
        try
        {
            AppGameContainer app = new AppGameContainer(new Wumpus("Wumpus movement rendering"));
            app.setDisplayMode(800, 700, false);
            app.setDisplayMode(app.getWidth(), app.getHeight(), false);
            app.setVSync(true);
            app.setAlwaysRender(true);
            
            
            app.start();
        }
        catch(Exception e)
        {
            System.out.println("Problem initializing the game");
            e.printStackTrace();
        }
    }
    
}
